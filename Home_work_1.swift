func checkPrimeNumber (number: Int) -> Bool {
    var result = false
    var index = number
    if index == 2 || index == 1 {
        return true 
    }
    while index > 2 {
        let check = number % (index-1)
            if check == 0 { 
            result = false 
            break
            } 
                else {
                result = true
                }
            index -= 1    
            }
    return result
}

func initArrayOfPrimeNumbers (maxBound: Int = 10) -> [Int] {
    var primeArray = [Int] ()
    var i = 1
    var count = 1
    switch maxBound {
        case 1...Int.max:
            while (count <= maxBound) {
                if checkPrimeNumber(number:i) {
                    primeArray.append (i)    
                    count += 1
                }
            i += 1
            }
        default : break
    }
    return primeArray
}

func createFiboToValue (maxBound: Int = 10) -> ([Int]) {
    var k = 0
    var l = 1
    var n = 1
    var summ = 0
    var fiboArray = [Int] ()
    switch maxBound {
        case Int.min...0: return []
        default:
            while n <= maxBound && summ >= 0 {
            fiboArray.append (summ)
            summ = k &+ l
            l = k
            k = summ
            n += 1
        }
    return fiboArray
    }
}

func createSquareNumbersToElements (maxBound: Int = 10) -> ([Int]) {
    var myArray : [Int] = [1]
    var count: Int = 2
    switch maxBound {
        case Int.min...0: 
            myArray.removeLast()
            return []
        default:
            while count <= maxBound && count&*count > 1 {
            myArray.append(count*count)
            count += 1
    }
    return myArray
    }
}

func addArrayToDictionary (name: String, array: [Int]) -> [String:[Int]] {
    var myDict = [String: [Int]] ()
    myDict = [name: array]
    return myDict 
}

func maxValueInCollection (collection:[String:[Int]]) -> ([String:Int]) {
var max = 0
var key = ""
var result = [String:Int]()
    for i in collection.values {
        for j in i {
            max = j
        }
    }
    for i in collection.keys {
        key = i
    }
    result = [key:max]
    return result
}

func getMaxValueInCollection (array:[[String:Int]]) -> (index:String, max:Int) {
var tuple = ("", Int())
var max = 0
var index = ""
    for i in array {
        for j in i {
            tuple = j
            let (key, value) = tuple
            if max < value {
                max = value
                index = key
            }
        }
    }
    return (index, max)
}

let myBound = -5

var myPrime = addArrayToDictionary(name: "Prime", array: initArrayOfPrimeNumbers(maxBound:myBound))
var myFibo = addArrayToDictionary(name: "Fibbo", array: createFiboToValue(maxBound:myBound))
var mySqrt = addArrayToDictionary(name: "Square", array: createSquareNumbersToElements(maxBound:myBound))

print (myPrime)
print (myFibo)
print (mySqrt)

let maxInPrime = maxValueInCollection(collection:myPrime)
let maxInFibo = maxValueInCollection(collection:myFibo)
let maxInSquare = maxValueInCollection(collection:mySqrt)

let wholeDictionary = [maxInPrime, maxInFibo, maxInSquare]

let result = getMaxValueInCollection(array:wholeDictionary)

print("Collectiont key '\(result.index)', max value is \(result.max)")















